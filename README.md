# chat_app

A simple chat application  developed by Flutter WebView.

## Getting Started

This app loads the chat pplication developed by laravel into a WebView and user can works with chat after login
in globals.dart the baseUrl for laravel application has benn set. If mobile device and the PC are connected to the same WiFi network they can connected
easily to each other. In my case the wifi IP address regarding ipconfig was 192.168.1.5, therefore I set the IP in globals.dart to 
**192.168.1.5:8000** in order to run the laravel app in flutter webview.
It should be mentioned I started the laravel app with the command : 
**php artisan serve --host 192.168.1.5 --port 8000**
