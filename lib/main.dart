import 'dart:async';
import 'globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() => runApp(MaterialApp(home: WebViewChat()));

class WebViewChat extends StatefulWidget {
  @override
  _WebViewChatState createState() => _WebViewChatState();
}

class _WebViewChatState extends State<WebViewChat> {
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter chat by Behdad'),

      ),

      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl: globals.baseUrl,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },

          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
        );
      }),
    );
  }



}

